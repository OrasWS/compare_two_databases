# Compare Two databases

This small script is to compare tables data between two databases. This script will basically:

- List all tables
- Get the primary key for tables
- Compare two tables by top value of the primary key.
- List queries that you need to run on Old database to get data that are not available in the new one.


## Scenario to use the script

Consider you have switched a system to a new platform but you found out that some systems are still connected to the old one during migration time. Running this code for both databases will list all tables that have been updated during this time with the exact query to get the newly inserted data.


## To Run

- `git clone git@bitbucket.org:OrasWS/compare_two_databases.git` repo
- Populate databases credentials (db1 => old database, db2 => new database)
- From Terminal, run `php index.php`
- Run `bash export_sql.sh`
- You'll have a `.sql` dump file with all data