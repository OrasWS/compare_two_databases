<?php
/**
 *  This script is to compare two databases and check if the old one has new data comparing to the new one.
 */

// populate database connections
$oldDatabase = [
'host' => '',
'user' => '',
'pass' => '',
'db'   => ''
];

$newDatabase = [
'host' => '',
'user' => '',
'pass' => '',
'db'   => ''
];


if(empty($oldDatabase) || empty($newDatabase)) {
	die("Please populate both old and new database credentials");
}

$oldDatabaseConnection = new Mysqli($oldDatabase['host'], $oldDatabase['user'], $oldDatabase['pass'], $oldDatabase['db']) or die('Can not connect to OLD Database');
$newDatabaseConnection = new Mysqli($newDatabase['host'], $newDatabase['user'], $newDatabase['pass'], $newDatabase['db']) or die('Can not connect to NEW Database');


$listTables = "show tables";

$listPrimaryKeyOfTables = "show index from %s where Key_name = 'PRIMARY'";

$result = $oldDatabaseConnection->query($listTables);

$noKey = [];
$changed = [];
while ( $row = $result->fetch_array()) {
	$tableName = $row[0];
	$primaryKey = sprintf($listPrimaryKeyOfTables, $tableName);

	$result2 = $oldDatabaseConnection->query($primaryKey);

	
	if($result2->num_rows > 0) {
		$prim = $result2->fetch_array();

		$queryToCompare = sprintf("SELECT %s FROM %s order by %s DESC limit 1", $prim['Column_name'], $tableName, $prim['Column_name']);

		$res1 = $oldDatabaseConnection->query($queryToCompare);
		$res2 = $newDatabaseConnection->query($queryToCompare);

		if(empty($res1) OR empty($res2)) continue;
		
			if($res1->num_rows > 0 && $res2->num_rows > 0) {
				$val1 = $res1->fetch_assoc();
				$val2 = $res2->fetch_assoc();

				if($val1 > $val2) {
					$changed[] = sprintf("mysqldump -h%s -u%s -p%s %s %s --where=\"%s > %s\" --no-create-info >> export_data.sql",$oldDatabase['host'], $oldDatabase['user'], $oldDatabase['pass'], $oldDatabase['db'], $tableName, $prim['Column_name'], $val2[$prim['Column_name']]);
				}
			}

	} else {
		$noKey[] = sprintf("Table [%s] has no primary key", $tableName).PHP_EOL;
	}
	
}

$line = implode("\n", $changed);
$bashScript = <<<BASH
#!/bin/bash
$line
BASH;
file_put_contents('export_sql.sh', $bashScript);
